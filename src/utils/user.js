import fetchWithTimeout from "../utils/fetchWithTimeout";
import { API_URL } from "./constants";

const userTimeout = 5000;

export const login = async (data) => {
  const res = await fetchWithTimeout(
    `${API_URL}/login`,
    {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    },
    userTimeout
  );
  return res;
};

export const register = async (data) => {
  const res = await fetchWithTimeout(
    `${API_URL}/register`,
    {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    },
    userTimeout
  );
  return res;
};

export const resetPasswordRequest = async (data) => {
  const res = await fetchWithTimeout(
    `${API_URL}/login/forgottenPassword`,
    {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    },
    userTimeout
  );
  return res;
};

export const resetPassword = async (id, data) => {
  const res = await fetchWithTimeout(
    `${API_URL}/login/forgottenPassword/${id}`,
    {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    },
    userTimeout
  );
  return res;
};

export const resetPasswordExists = async (id) => {
  const res = await fetchWithTimeout(
    `${API_URL}/login/forgottenPassword/${id}`,
    {
      method: "GET",
      headers: { "Content-Type": "application/json" },
    },
    userTimeout
  );
  return res;
};

export const getUser = async (username, token) => {
  const res = await fetchWithTimeout(
    `${API_URL}/users/${username}`,
    {
      method: "GET",
      headers: { "Content-Type": "application/json", "auth-token": token },
    },
    userTimeout
  );
  return res;
};

export const subscribeToPost = async (username, postId, action = "subscribe", token) => {
  const res = await fetchWithTimeout(
    `${API_URL}/users/${username}/subscribe/${postId}`,
    {
      method: "PATCH",
      headers: { "Content-Type": "application/json", "auth-token": token },
      body: JSON.stringify({ action: action }),
    },
    userTimeout
  );
  return res;
};

export const subscribeToTag = async (username, tagId, action = "subscribe", token) => {
  const res = await fetchWithTimeout(
    `${API_URL}/users/${username}/tag/${tagId}`,
    {
      method: "PATCH",
      headers: { "Content-Type": "application/json", "auth-token": token },
      body: JSON.stringify({ action: action }),
    },
    userTimeout
  );
  return res;
};

export const updateUser = async (username, body, authToken) => {
  let user = await fetchWithTimeout(
    `${API_URL}/users/${username}`,
    {
      method: "PATCH",
      headers: { "Content-Type": "application/json", "auth-token": authToken },
      body: JSON.stringify(body),
    },
    userTimeout
  );
  return user;
};

export const checkPassword = async (username, body, authToken) => {
  let user = await fetchWithTimeout(
    `${API_URL}/users/checkPassword/${username}`,
    {
      method: "POST",
      headers: { "Content-Type": "application/json", "auth-token": authToken },
      body: JSON.stringify(body),
    },
    userTimeout
  );
  return user;
};
