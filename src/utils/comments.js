import fetchWithTimeout from "../utils/fetchWithTimeout";
import { API_URL } from "./constants";

const commentTimeout = 5000;

export const getCommentsFromPost = async (postId, authToken) => {
  let comment = await fetchWithTimeout(
    `${API_URL}/comments/post/${postId}`,
    {
      method: "GET",
      headers: { "Content-Type": "application/json", "auth-token": authToken },
    },
    commentTimeout
  );
  return comment;
};

export const deleteComment = async (commentId, authToken) => {
  let comment = await fetchWithTimeout(
    `${API_URL}/comments/${commentId}`,
    {
      method: "DELETE",
      headers: { "Content-Type": "application/json", "auth-token": authToken },
    },
    commentTimeout
  );
  return comment;
};

export const createComment = async (body, authToken) => {
  let comment = await fetchWithTimeout(
    `${API_URL}/comments/`,
    {
      method: "POST",
      headers: { "Content-Type": "application/json", "auth-token": authToken },
      body: JSON.stringify(body),
    },
    commentTimeout
  );
  return comment;
};

export const getAllCommentsByAuthor = async (username, authToken) => {
  let comments = await fetchWithTimeout(
    `${API_URL}/comments/user/${username}`,
    {
      method: "GET",
      headers: { "Content-Type": "application/json", "auth-token": authToken },
    },
    commentTimeout
  );
  return comments;
};
