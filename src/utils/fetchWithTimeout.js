export default async function fetchWithTimeout(url, options, delay = 10000) {
  try {
    const timer = new Promise((resolve) => {
      setTimeout(resolve, delay, {
        timeout: true,
      });
    });
    const response = await Promise.race([
      fetch(url, options).then((res) => {
        if (res.status === 204) return res;
        if (res.status >= 400) {
          return res.json().then((msg) => {
            return { error: msg, errorCode: res.status };
          });
        } else return res.json();
      }),
      timer,
    ]);
    if (response.timeout) {
      console.error("Server Timeout");
      return { error: "Server error, please check server status" };
    }
    return response;
  } catch (e) {
    return { error: `Problème de connexion : ${e}` };
  }
}
