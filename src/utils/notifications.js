import fetchWithTimeout from "../utils/fetchWithTimeout";
import { API_URL } from "./constants";

const notificationTimeout = 5000;

export const getAllNotificationsByUsername = async (username, authToken) => {
  let notifications = await fetchWithTimeout(
    `${API_URL}/notifications/username/${username}`,
    {
      method: "GET",
      headers: { "Content-Type": "application/json", "auth-token": authToken },
    },
    notificationTimeout
  );
  return notifications;
};
