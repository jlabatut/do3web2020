import fetchWithTimeout from "../utils/fetchWithTimeout";
import { API_URL } from "./constants";

const postTimeout = 5000;

export const getPostsList = async (list, authToken) => {
  const promises = list.map((post) =>
    fetchWithTimeout(
      `${API_URL}/posts/${post}`,
      {
        method: "GET",
        headers: { "Content-Type": "application/json", "auth-token": authToken },
      },
      postTimeout
    )
  );
  return await Promise.all(promises);
};

export const createPost = async (body, authToken) => {
  let post = await fetchWithTimeout(
    `${API_URL}/posts/`,
    {
      method: "POST",
      headers: { "Content-Type": "application/json", "auth-token": authToken },
      body: JSON.stringify(body),
    },
    postTimeout
  );
  return post;
};

export const getAllPosts = async (authToken) => {
  let post = await fetchWithTimeout(
    `${API_URL}/posts/`,
    {
      method: "GET",
      headers: { "Content-Type": "application/json", "auth-token": authToken },
    },
    postTimeout
  );
  return post;
};

export const getPost = async (id, authToken) => {
  let post = await fetchWithTimeout(
    `${API_URL}/posts/${id}`,
    {
      method: "GET",
      headers: { "Content-Type": "application/json", "auth-token": authToken },
    },
    postTimeout
  );
  return post;
};

export const deletePost = async (id, authToken) => {
  let post = await fetchWithTimeout(
    `${API_URL}/posts/${id}`,
    {
      method: "DELETE",
      headers: { "Content-Type": "application/json", "auth-token": authToken },
    },
    postTimeout
  );
  return post;
};

export const getAllPostsByTags = async (tags, authToken) => {
  let tagsList = "?";
  for (let tag of tags) {
    tagsList += `tag=${tag}&`;
  }
  let post = await fetchWithTimeout(
    `${API_URL}/posts/tags${tagsList}`,
    {
      method: "GET",
      headers: { "Content-Type": "application/json", "auth-token": authToken },
    },
    postTimeout
  );
  return post;
};

export const updatePost = async (postId, body, authToken) => {
  let post = await fetchWithTimeout(
    `${API_URL}/posts/${postId}`,
    {
      method: "PATCH",
      headers: { "Content-Type": "application/json", "auth-token": authToken },
      body: JSON.stringify(body),
    },
    postTimeout
  );
  return post;
};

export const getAllPostsByAuthor = async (username, authToken) => {
  let posts = await fetchWithTimeout(
    `${API_URL}/posts/user/${username}`,
    {
      method: "GET",
      headers: { "Content-Type": "application/json", "auth-token": authToken },
    },
    postTimeout
  );
  return posts;
};
