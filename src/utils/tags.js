import fetchWithTimeout from "../utils/fetchWithTimeout";
import { API_URL } from "./constants";

const tagTimeout = 5000;

export const getAllTags = async (token) => {
  const res = await fetchWithTimeout(
    `${API_URL}/tags`,
    {
      method: "GET",
      headers: { "Content-Type": "application/json", "auth-token": token },
    },
    tagTimeout
  );
  return res;
};

export const getTag = async (tagId, token) => {
  const res = await fetchWithTimeout(
    `${API_URL}/tags/${tagId}`,
    {
      method: "GET",
      headers: { "Content-Type": "application/json", "auth-token": token },
    },
    tagTimeout
  );
  return res;
};
