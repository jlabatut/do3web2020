import React, { useContext, useState, useEffect } from "react";
import { Redirect } from "react-router-dom";

import { updatePost, getPost } from "../../utils/posts";
import { Context } from "../Store";
import PostForm from "../utilityComponents/PostForm";

export default function UpdatePost(props) {
  const [state] = useContext(Context);
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const [tags, setTags] = useState([]);
  const [dataToSend, setDataToSend] = useState({});
  const [TagsIdList, setTagsIdList] = useState([]);
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [redirect, setRedirect] = useState("");
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    if (props.match && props.match.params && props.match.params.id) {
      setIsLoading(true);
      getPost(props.match.params.id, state.token).then((data) => {
        setIsLoading(false);
        if (data.error) {
          if (data.errorCode && data.errorCode === 404) {
            return setRedirect(`/idea/${props.match.params.id}`);
          }
          return setError(data.error);
        }
        if (data.post.author._id !== state.user._id) return setRedirect(`/idea/${props.match.params.id}`);
        setTitle(data.post.title);
        setContent(data.post.content);
        setTags(data.post.tags);
        document.title = data.post.title + " - Coding Ideas";
        setTagsIdList(data.post.tags.map((tag) => tag._id));
        setIsLoaded(true);
      });
    } else {
      return setRedirect("/ideas");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.match?.params?.id]);

  const sendPost = async () => {
    setIsLoading(true);
    let post = await updatePost(props.match.params.id, dataToSend, state.token);
    if (post.error) {
      setIsLoading(false);
      return setError(post.error);
    }
    let d = new Date();
    d.setTime(d.getTime() + 2000);
    document.cookie = `PostInfos=Votre idée a bien été modifiée !;expires=${d.toUTCString()};path=/`;
    setIsLoading(false);
    return setRedirect(`/idea/${props.match.params.id}`);
  };

  return (
    <div>
      {redirect ? <Redirect to={redirect} /> : null}
      <div id="title" className="py-5 break-words">
        <h1 className="text-3xl sm:text-5xl">Modifiez votre idée</h1>
      </div>
      <div className="flex justify-center">
        <div className="w-full sm:w-4/5 ">
          {isLoaded && (
            <PostForm
              token={state.token}
              onSubmit={sendPost}
              error={error}
              setError={setError}
              isLoading={isLoading}
              setIsLoading={setIsLoading}
              setRedirect={setRedirect}
              setDataToSend={setDataToSend}
              postTitle={title}
              postContent={content}
              postTags={tags}
              TagsIdList={TagsIdList}
              isEditing={true}
            />
          )}
        </div>
      </div>
    </div>
  );
}
