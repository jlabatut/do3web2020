import React, { useContext, useState } from "react";

import { login } from "../../utils/user";
import { Context } from "../Store";
import { regexEmail, regexUsername, regexPassword } from "../../utils/constants";

import Logo from "../../assets/Logo";
import Spinner from "../../assets/Spinner";
import Message from "../utilityComponents/Message";

export default function Login(props) {
  const [, setState] = useContext(Context);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const [LoginInfos, setLoginInfos] = useState(() => {
    if (document.cookie && document.cookie.includes("LoginInfos")) {
      const value = `; ${document.cookie}`;
      const parts = value.split(`; LoginInfos=`);
      if (parts.length === 2) {
        if (parts[1].includes("Un e-mail de réinitialisation")) {
          let splitted = parts[1].split(" ");
          setUsername(splitted[splitted.length - 1]);
        }
        return parts.pop().split(";").shift();
      }
    } else return "";
  });

  const log = async () => {
    if (!username.match(regexEmail) && !username.match(regexUsername))
      return setError("L'adresse e-mail ou le nom d'utilisateur doit être valide");
    if (!password.match(regexPassword)) return setError("Le format du mot de passe n'est pas valide");
    setIsLoading(true);
    document.getElementById("submitBtn").disabled = true;
    let myLogin = await login({ username, password });
    setIsLoading(false);
    document.getElementById("submitBtn").disabled = false;
    if (myLogin.error) {
      if (myLogin?.error?.error) return setError(myLogin.error.error);
      setError(myLogin.error);
      if (LoginInfos) setLoginInfos("");
      if (error !== "") {
        document.getElementById("errorMsg")?.classList.add("animate-bounce");
        await new Promise((r) => setTimeout(r, 1500));
        document.getElementById("errorMsg")?.classList.remove("animate-bounce");
      }
    } else {
      localStorage.setItem("user", JSON.stringify(myLogin.user));
      localStorage.setItem("token", myLogin.token);
      setState(myLogin);
    }
  };

  document.title = "Login - Coding Ideas";

  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-50 py-2 px-4 sm:px-6 lg:px-8">
      <div className="max-w-lg w-full space-y-8">
        <div>
          <h1 className="font-black text-4xl md:text-6xl text-purple-700 mb-4">Coding Ideas</h1>
          <Logo className="block center m-auto w-auto sm:w-64 h-24 sm:h-30" />
          <h2 className="mt-6 text-center text-2xl md:text-3xl font-extrabold text-gray-900">
            Connectez-vous à votre compte
          </h2>
          <p className="mt-2 text-center text-sm font-medium text-gray-600">et découvrez des milliers d'idées</p>
        </div>
        <form
          className="mt-8 space-y-6"
          onSubmit={(e) => {
            e.preventDefault();
            log();
          }}
        >
          <div className="rounded-md shadow-sm -space-y-px">
            <div>
              <label htmlFor="email-address" className="sr-only">
                Nom d'utilisateur ou adresse e-mail
              </label>
              <input
                id="email-address"
                name="email"
                type="text"
                autoComplete="username"
                required
                minLength="5"
                title="Le nom d'utilisateur ou l'adresse e-mail doivent être valides"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="Nom d'utilisateur ou adresse e-mail"
              />
            </div>
            <div>
              <label htmlFor="password" className="sr-only">
                Mot de passe
              </label>
              <input
                id="password"
                name="password"
                type="password"
                autoComplete="current-password"
                required
                pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$"
                title="Le mot de passe doit être valide"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="Mot de passe"
              />
            </div>
          </div>
          {error ? <Message message={error} setError={setError} /> : null}
          {LoginInfos ? <Message message={LoginInfos} setError={setLoginInfos} type="success" /> : null}
          <div className="flex items-center justify-between">
            <div className="text-sm">
              <a href="/resetPassword" className="font-medium text-purple-700">
                Mot de passe oublié ?
              </a>
            </div>
            <div className="text-sm">
              <a href="/register" className="font-medium text-purple-700">
                Créer un compte
              </a>
            </div>
          </div>

          <div>
            <button
              type="submit"
              id="submitBtn"
              className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-purple-700 hover:bg-purple-800 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              <span className="absolute left-0 inset-y-0 flex items-center pl-3">
                <svg
                  className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                  aria-hidden="true"
                >
                  <path
                    fillRule="evenodd"
                    d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z"
                    clipRule="evenodd"
                  />
                </svg>
              </span>
              {isLoading ? <Spinner size="20" /> : "Connexion"}
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
