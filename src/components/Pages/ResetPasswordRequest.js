import React, { useState } from "react";
import { Redirect } from "react-router-dom";

import { resetPasswordRequest } from "../../utils/user";
import { regexEmail } from "../../utils/constants";

import Logo from "../../assets/Logo";
import Spinner from "../../assets/Spinner";
import Message from "../utilityComponents/Message";

export default function ResetPasswordRequest(props) {
  const [email, setEmail] = useState("");
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [redirect, setRedirect] = useState(false);

  const resetMyPassword = async () => {
    if (!email.match(regexEmail)) return setError("L'adresse e-mail doit être valide");
    setIsLoading(true);
    document.getElementById("submitBtn").disabled = true;
    let myResetPasswordRequest = await resetPasswordRequest({ email });
    setIsLoading(false);
    document.getElementById("submitBtn").disabled = false;
    if (myResetPasswordRequest.error) {
      if (myResetPasswordRequest.error.error) return setError(myResetPasswordRequest.error.error);
      setError(myResetPasswordRequest.error);
      if (error !== "") {
        document.getElementById("errorMsg").classList.add("animate-bounce");
        await new Promise((r) => setTimeout(r, 1500));
        document.getElementById("errorMsg").classList.remove("animate-bounce");
      }
    } else {
      var d = new Date();
      d.setTime(d.getTime() + 2000);
      document.cookie = `LoginInfos=Un e-mail de réinitialisation du mot de passe a été envoyé à ${email};expires=${d.toUTCString()};path=/`;
      setRedirect(true);
    }
  };

  document.title = "Password reset request - Coding Ideas";

  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-50 py-2 px-4 sm:px-6 lg:px-8">
      {redirect ? <Redirect push to="/login" /> : null}
      <div className="max-w-lg w-full space-y-8">
        <div>
          <h1 className="font-black text-4xl md:text-6xl text-purple-700 mb-4">Coding Ideas</h1>
          <Logo className="block center m-auto w-auto sm:w-64 h-24 sm:h-30" />
          <h2 className="mt-6 text-center text-2xl md:text-3xl font-extrabold text-gray-900">
            Récupérez votre mot de passe
          </h2>
          <p className="mt-2 text-center text-sm font-medium text-gray-600">
            en indiquant l'adresse avec laquelle vous vous êtes inscrit
          </p>
        </div>
        <form
          className="mt-8 space-y-6"
          onSubmit={(e) => {
            e.preventDefault();
            resetMyPassword();
          }}
        >
          <div className="rounded-md shadow-sm -space-y-px">
            <div>
              <label htmlFor="email-address" className="sr-only">
                Adresse e-mail
              </label>
              <input
                id="email-address"
                name="email"
                type="email"
                autoComplete="email"
                required
                title="L'e-mail doit être correct"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="Adresse e-mail"
              />
            </div>
          </div>
          {error ? <Message message={error} setError={setError} /> : null}
          <div className="flex items-center justify-between">
            <div className="text-sm">
              <a href="/login" className="font-medium text-purple-700">
                Retour à la connexion
              </a>
            </div>
          </div>

          <div>
            <button
              type="submit"
              id="submitBtn"
              className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-purple-700 hover:bg-purple-800 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              {isLoading ? <Spinner size="20" /> : "Réinitialiser mon mot de passe"}
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
