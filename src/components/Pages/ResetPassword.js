import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";

import { resetPassword, resetPasswordExists } from "../../utils/user";
import { regexPassword } from "../../utils/constants";

import Logo from "../../assets/Logo";
import Spinner from "../../assets/Spinner";
import Message from "../utilityComponents/Message";

export default function ResetPassword(props) {
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [redirect, setRedirect] = useState("");

  useEffect(() => {
    (async function exists() {
      let isURLExists = await resetPasswordExists(props.match.params.id);
      if (isURLExists.error) setRedirect("/invalidNewPasswordToken");
    })();
  }, [props.match.params.id]);

  const resetMyPassword = async () => {
    if (!password.match(regexPassword))
      return setError(
        "Le mot de passe doit contenir au moins 8 caractères, dont un chiffre, une majuscule et une minuscule"
      );
    if (password !== password2) return setError("Les mots de passe doivent correspondre !");
    setIsLoading(true);
    document.getElementById("submitBtn").disabled = true;
    let myResetPassword = await resetPassword(props.match.params.id, { password });
    setIsLoading(false);
    document.getElementById("submitBtn").disabled = false;

    if (myResetPassword.error) {
      setError(myResetPassword.error);
      if (error !== "") {
        document.getElementById("errorMsg").classList.add("animate-bounce");
        await new Promise((r) => setTimeout(r, 1500));
        document.getElementById("errorMsg").classList.remove("animate-bounce");
      }
    } else {
      var d = new Date();
      d.setTime(d.getTime() + 2000);
      document.cookie = `LoginInfos=Votre mot de passe a bien été changé;expires=${d.toUTCString()};path=/`;
      setRedirect("/login");
    }
  };

  document.title = "Password reset - Coding Ideas";

  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-50 py-2 px-4 sm:px-6 lg:px-8">
      {redirect ? <Redirect push to={redirect} /> : null}
      <div className="max-w-lg w-full space-y-8">
        <div>
          <h1 className="font-black text-4xl md:text-6xl text-purple-700 mb-4">Coding Ideas</h1>
          <Logo className="block center m-auto w-auto sm:w-64 h-24 sm:h-30" />
          <h2 className="mt-6 text-center text-2xl md:text-3xl font-extrabold text-gray-900">
            Réinitialisez votre mot de passe
          </h2>
          <p className="mt-2 text-center text-sm font-medium text-gray-600">
            Indiquez votre nouveau mot de passe ci-dessous
          </p>
        </div>
        <form
          className="mt-8 space-y-6"
          onSubmit={(e) => {
            e.preventDefault();
            resetMyPassword();
          }}
        >
          <div className="rounded-md shadow-sm -space-y-px">
            <div>
              <label htmlFor="password" className="sr-only">
                Mot de passe
              </label>
              <input
                id="password"
                name="pasword"
                type="password"
                autoComplete="password"
                required
                pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$"
                title="Le mot de passe doit contenir au moins 8 caractères, dont un chiffre, une majuscule et une minuscule"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="Mot de passe"
              />
            </div>
            <div>
              <label htmlFor="password2" className="sr-only">
                Confirmez le mot de passe
              </label>
              <input
                id="password2"
                name="pasword2"
                type="password"
                autoComplete="password"
                required
                pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$"
                title="Le mot de passe doit contenir au moins 8 caractères, dont un chiffre, une majuscule et une minuscule"
                value={password2}
                onChange={(e) => setPassword2(e.target.value)}
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="Confirmez le mot de passe"
              />
            </div>
          </div>
          {error ? <Message message={error} setError={setError} /> : null}
          <div>
            <button
              type="submit"
              id="submitBtn"
              className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-purple-700 hover:bg-purple-800 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              {isLoading ? <Spinner size="20" /> : "Réinitialiser mon mot de passe"}
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
