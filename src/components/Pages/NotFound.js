import React from "react";
import { Link } from "react-router-dom";

export default function NotFound() {
  document.title = "404 Page Not Found - Coding Ideas";
  return (
    <div>
      <div className="py-7">
        <h1 className="text-5xl uppercase font-bold">404 - Page Not Found</h1>

        <div className="animate-bounce pt-7 text-purple-700 uppercase font-bold text-3xl">
          <Link to="/">Go Home</Link>
        </div>
      </div>
      {/* <div className="flex justify-center">
        <img
          className="rounded-full w-60 md:w-80"
          src="https://i.pinimg.com/originals/97/89/0d/97890dfad655a59dbf7d4466336ca49e.jpg"
          alt="Vald est tombé sur une page qui n'existe pas, il est perdu."
          title="Vald est tombé sur une page qui n'existe pas, il est perdu."
        />
      </div> */}
    </div>
  );
}
