import React, { useContext, useState } from "react";
import { Redirect } from "react-router-dom";

import { createPost } from "../../utils/posts";
import { Context } from "../Store";
import PostForm from "../utilityComponents/PostForm";

export default function CreatePost() {
  const [state] = useContext(Context);
  const [dataToSend, setDataToSend] = useState({});
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [redirect, setRedirect] = useState("");

  document.title = "Nouvelle idée - Coding Ideas";

  const sendPost = async () => {
    setIsLoading(true);
    let post = await createPost(dataToSend, state.token);
    if (post.error) {
      setIsLoading(false);
      return setError(post.error);
    }
    let d = new Date();
    d.setTime(d.getTime() + 2000);
    document.cookie = `PostInfos=Votre idée a bien été publiée !;expires=${d.toUTCString()};path=/`;
    setIsLoading(false);
    return setRedirect(`/idea/${post.post._id}`);
  };

  return (
    <div>
      {redirect ? <Redirect to={redirect} /> : null}
      <div id="title" className="py-5 break-words">
        <h1 className="text-3xl sm:text-5xl">Proposez votre nouvelle idée !</h1>
      </div>
      <div className="flex justify-center">
        <div className="w-full sm:w-4/5 ">
          <PostForm
            token={state.token}
            onSubmit={sendPost}
            error={error}
            setError={setError}
            isLoading={isLoading}
            setIsLoading={setIsLoading}
            setRedirect={setRedirect}
            setDataToSend={setDataToSend}
          />
        </div>
      </div>
    </div>
  );
}
