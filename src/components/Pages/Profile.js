import React, { useEffect, useState, useContext } from "react";

import { Context } from "../Store";
import { getUser, updateUser, checkPassword } from "../../utils/user";
import { getAllPostsByAuthor } from "../../utils/posts";
import { getAllCommentsByAuthor } from "../../utils/comments";
import { regexEmail, regexPassword } from "../../utils/constants";

import Message from "../utilityComponents/Message";
import NotFound from "./NotFound";
import Spinner from "../../assets/Spinner";
import { Link } from "react-router-dom";

export default function Profile(props) {
  const [state, setState] = useContext(Context);
  const [user, setUser] = useState({});
  const [posts, setPosts] = useState([]);
  const [comments, setComments] = useState([]);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [newPassword1, setNewPassword1] = useState("");
  const [newPassword2, setNewPassword2] = useState("");
  const [isOwner, setIsOwner] = useState(false);
  const [error, setError] = useState("");
  const [info, setInfo] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [isLoadingUpdateUser, setIsLoadingUpdateUser] = useState(false);
  const [isNotFound, setIsNotFound] = useState(false);

  useEffect(() => {
    setIsNotFound(false);
    if (props.match.params.username === state.user.username) {
      setIsOwner(true);
      setEmail(state.user.email);
      setUser(state.user);
    } else {
      setIsOwner(false);
      setIsLoading(true);
      getUser(props.match.params.username, state.token)
        .then((user) => {
          setIsLoading(false);
          if (user.error) {
            if (user.errorCode && user.errorCode === 404) {
              return setIsNotFound(true);
            }
            return setError(user.error);
          }
          setUser(user.user);
        })
        .catch((error) => {
          setError(error);
        });
    }
    if (user.username) {
      getAllPostsByAuthor(user.username, state.token)
        .then((posts) => {
          if (posts.error) {
            if (posts.errorCode && posts.errorCode === 404) {
              return setIsNotFound(true);
            }
            return setError(posts.error);
          }
          setPosts(posts.posts);
        })
        .catch((e) => setError(e));
      getAllCommentsByAuthor(user.username, state.token)
        .then((comments) => {
          if (comments.error) {
            if (comments.errorCode && comments.errorCode === 404) {
              return setIsNotFound(true);
            }
            return setError(comments.error);
          }
          setComments(comments.comments);
        })
        .catch((e) => setError(e));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.match.params.username, state, setState]);

  const editUser = async () => {
    if (!email.match(regexEmail)) return setError("L'adresse e-mail doit être valide");
    if (!password.match(regexPassword))
      return setError(
        "Le mot de passe doit contenir au moins 8 caractères, dont un chiffre, une majuscule et une minuscule"
      );
    if (newPassword1 !== newPassword2) return setError("Les deux mots de passe doivent correspondre");
    try {
      setIsLoadingUpdateUser(true);
      let isPasswordCorrect = await checkPassword(props.match.params.username, { password: password }, state.token);
      if (!isPasswordCorrect.password) {
        setIsLoadingUpdateUser(false);
        return setError("Le mot de passe est incorrect");
      }
      let editUser = await updateUser(
        props.match.params.username,
        { email, ...(newPassword1.length >= 3 && { password: newPassword1 }) },
        state.token
      );
      setIsLoadingUpdateUser(false);
      if (editUser?.error?.error) return setError(editUser.error.error);
      if (editUser.error) return setError(editUser.error);
      setState({ user: editUser, token: state.token });
      setError("");
      setPassword("");
      setNewPassword1("");
      setNewPassword2("");
      setInfo("Votre profil a été mis à jour !");
      return null;
    } catch (e) {
      setError(e);
    }
  };

  return (
    <div>
      {info ? <Message message={info} type="success" setError={setInfo} /> : null}
      {error.length !== 0 ? <Message message={error} setError={setError} /> : null}
      {isNotFound ? (
        <NotFound />
      ) : (
        <div>
          <div className="flex justify-center">
            <div className="w-full sm:w-4/5 mb-5">
              <div id="homeTitle" className="py-5 break-words text-left">
                <h1 className="text-3xl sm:text-5xl font-extrabold uppercase mx-5 sm:mx-0">
                  {props.match.params.username}
                </h1>
              </div>
              {!isLoading ? (
                <div className="flex flex-wrap justify-center sm:justify-between">
                  {isOwner && (
                    <div className="w-1/2 min-w-max flex flex-col justify-center mb-2 bg-gray-200 rounded-lg p-4">
                      <div className="font-bold">Modifier mes informations</div>
                      <div className="mt-4 max-w-xs flex flex-col m-auto">
                        <div className="rounded-md shadow-sm flex">
                          <form
                            id="updateUserForm"
                            onSubmit={(e) => {
                              e.preventDefault();
                              editUser();
                            }}
                          >
                            <div>
                              <label htmlFor="email-address" className="sr-only">
                                Adresse e-mail
                              </label>
                              <input
                                id="email-address"
                                name="email"
                                type="email"
                                autoComplete="email"
                                required
                                title="L'adresse e-mail doit être valide'"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                                className="appearance-none rounded-md relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                                placeholder="Adresse e-mail"
                              />
                            </div>
                            <div className="mt-3">
                              <label htmlFor="newPassword" className="sr-only">
                                Nouveau mot de passe
                              </label>
                              <input
                                id="newPassword"
                                name="newPassword"
                                type="password"
                                pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$"
                                title="Le mot de passe doit contenir au moins 8 caractères, dont un chiffre, une majuscule et une minuscule"
                                value={newPassword1}
                                onChange={(e) => setNewPassword1(e.target.value)}
                                className="appearance-none rounded-md relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                                placeholder="Nouveau mot de passe"
                              />
                            </div>
                            <div className="mt-3">
                              <label htmlFor="newPassword2" className="sr-only">
                                Confirmation du mot de passe
                              </label>
                              <input
                                id="newPassword2"
                                name="newPassword2"
                                type="password"
                                pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$"
                                title="Le mot de passe doit contenir au moins 8 caractères, dont un chiffre, une majuscule et une minuscule"
                                value={newPassword2}
                                onChange={(e) => setNewPassword2(e.target.value)}
                                className="appearance-none rounded-md relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                                placeholder="Confirmez le nouveau mot de passe"
                              />
                            </div>
                            <div className="mt-3">
                              <label htmlFor="password" className="sr-only">
                                Mot de passe actuel
                              </label>
                              <input
                                id="password"
                                name="password"
                                type="password"
                                required
                                pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$"
                                title="Le mot de passe doit contenir au moins 8 caractères, dont un chiffre, une majuscule et une minuscule"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                                className="appearance-none rounded-md relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                                placeholder="Mot de passe actuel"
                              />
                            </div>
                          </form>
                        </div>
                        <button
                          type="submit"
                          id="submitBtn"
                          form="updateUserForm"
                          className="group relative flex justify-center w-44 mx-auto my-4 py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-purple-700 hover:bg-purple-800 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                        >
                          {isLoadingUpdateUser ? <Spinner size="20" /> : "Modifier mon profil"}
                        </button>
                      </div>
                    </div>
                  )}
                  <div className="w-10/12 sm:w-1/2 min-w-max flex flex-col h-full">
                    <div className="bg-gray-200 rounded-lg">
                      <div className="font-bold">Statistiques</div>
                      <div className="">
                        <ul>
                          <li>Idées publiées - {posts.length}</li>
                          <li>Commentaires publiés - {comments.length}</li>
                        </ul>
                      </div>
                      <div className="flex justify-center">
                        <Link to={`/profile/${props.match.params.username}/ideas`}>
                          <button
                            type="submit"
                            className="group relative flex justify-center w-52 mx-auto my-4 py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-purple-700 hover:bg-purple-800 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                          >
                            Voir toutes ses idées
                          </button>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              ) : (
                <div className="justify-center flex mt-20">
                  <Spinner size="35" color="706FD3" />
                </div>
              )}
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
