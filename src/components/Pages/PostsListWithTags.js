import React, { useContext, useState, useEffect } from "react";

import { getAllPostsByTags } from "../../utils/posts";
import { subscribeToTag, getUser } from "../../utils/user";
import { Context } from "../Store";
import Spinner from "../../assets/Spinner";
import IdeaCard from "../utilityComponents/IdeaCard";
import Message from "../utilityComponents/Message";
import TagSearch from "../utilityComponents/TagSearch";
import NotFound from "./NotFound";
import { Redirect } from "react-router-dom";

export default function PostsList(props) {
  const [state, setState] = useContext(Context);
  const [ideas, setIdeas] = useState([]);
  const [isNotFound, setIsNotFound] = useState(false);
  const [URLTagsList, setUrlTagsList] = useState([]);
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [redirect, setRedirect] = useState("");

  useEffect(() => {
    setRedirect("");
    let tagsId = props.match.params.id.split("+");
    setUrlTagsList(tagsId);
    /* OK */
    getAllPostsByTags(tagsId, state.token).then((posts) => {
      setIsLoading(false);
      if (posts.error) {
        if (posts.errorCode && posts.errorCode === 404) {
          return setIsNotFound(true);
        }
        return setError(posts.error);
      }
      setIdeas(
        posts.posts.sort((x, y) => {
          return Date.parse(y.createdAt) - Date.parse(x.createdAt);
        })
      );
    });
    setIsLoading(true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.match.params.id, state]);

  const toggleSubscribeToTag = async () => {
    document.getElementById("subscribeBtn").disabled = true;
    let action = state.user.tags.includes(URLTagsList[0]) ? "unsubscribe" : "subscribe";
    let resSub = await subscribeToTag(state.user.username, URLTagsList[0], action, state.token);
    if (resSub.error) {
      setError("");
      if (resSub?.error?.error) return setError(resSub.error.error);
      return setError(resSub.error);
    }
    let updateUser = await getUser(state.user.username, state.token);
    if (updateUser.error) {
      setError("");
      if (updateUser.error.error) return setError(updateUser.error.error);
      return setError(updateUser.error);
    }
    setState({ user: updateUser.user, token: state.token });
    document.getElementById("subscribeBtn").disabled = false;
  };

  document.title = "Les dernières idées - Coding Ideas";

  return (
    <div className="mt-6">
      {redirect ? <Redirect to={redirect} /> : null}
      {isNotFound ? (
        <NotFound />
      ) : (
        <div>
          <div id="title" className="pb-5 break-words">
            <h1 className="text-3xl sm:text-4xl mb-4">Les dernières idées proposées</h1>
          </div>
          <div className="flex justify-center mb-5">
            <div className="w-11/12 sm:w-4/5">
              <TagSearch
                token={state.token}
                URLTagsList={URLTagsList}
                setError={setError}
                setIsLoading={setIsLoading}
                setRedirect={setRedirect}
              />
              {ideas.length === 0 && (
                <div className="mt-3 text-lg font-extrabold">Aucune idée ne correspond à votre recherche.</div>
              )}
              {error.length !== 0 ? <Message message={error} setError={setError} /> : null}
              {URLTagsList.length === 1 && (
                <div className="mt-3 mb-4 flex justify-center">
                  <button
                    id="subscribeBtn"
                    onClick={toggleSubscribeToTag}
                    className={`shadow flex focus:outline-none justify-center border-2 rounded-lg cursor-pointer px-1 py-1 my-2 mx-3 hover:text-white font-bold ${
                      state.user.tags.includes(URLTagsList[0])
                        ? "border-green-600 text-green-600 hover:bg-green-300"
                        : "border-purple-900 text-purple-900 hover:bg-purple-700 animate-pulse"
                    }`}
                  >
                    {state.user.tags.includes(URLTagsList[0])
                      ? "Vous êtes abonné à ce tag"
                      : "S'abonner aux nouvelles idées liées à ce tag"}
                  </button>
                </div>
              )}

              {!isLoading ? (
                ideas.map((post) => {
                  return post.error ? (
                    setError(post.error)
                  ) : (
                    <div key={post._id} className="mb-6">
                      <IdeaCard post={post} />
                    </div>
                  );
                })
              ) : (
                <div className="justify-center flex mt-20">
                  <Spinner size="35" color="706FD3" />
                </div>
              )}
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
