import React, { useContext, useState, useEffect } from "react";

import { getAllPostsByAuthor } from "../../utils/posts";
import { Context } from "../Store";

import IdeaCard from "../utilityComponents/IdeaCard";
import NotFound from "./NotFound";
import Spinner from "../../assets/Spinner";
import Message from "../utilityComponents/Message";

export default function PostsList(props) {
  const [state] = useContext(Context);
  const [ideas, setIdeas] = useState([]);
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [isNotFound, setIsNotFound] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    getAllPostsByAuthor(props.match.params.username, state.token).then((posts) => {
      setIsLoading(false);
      if (posts.error) {
        if (posts.errorCode && posts.errorCode === 404) {
          return setIsNotFound(true);
        }
        return setError(posts.error);
      }
      setIdeas(
        posts.posts.sort((x, y) => {
          return Date.parse(y.createdAt) - Date.parse(x.createdAt);
        })
      );
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.match.params.username]);

  document.title = `Les idées de ${props.match.params.username} - Coding Ideas`;

  return (
    <div className="mt-6">
      {isNotFound ? (
        <NotFound />
      ) : (
        <div>
          <div id="title" className="pb-5 break-words">
            <h1 className="text-3xl sm:text-4xl">Les idées de {props.match.params.username}</h1>
          </div>
          <div className="flex justify-center mb-5">
            <div className="w-11/12 sm:w-4/5">
              {error.length !== 0 ? <Message message={error} setError={setError} /> : null}
              {!isLoading ? (
                ideas.length === 0 ? (
                  <div>{props.match.params.username}&nbsp;n'a pas encore publié d'idées.</div>
                ) : (
                  ideas.map((post) => {
                    return post.error ? (
                      setError(post.error)
                    ) : (
                      <div key={post._id} className="mb-6">
                        <IdeaCard post={post} />
                      </div>
                    );
                  })
                )
              ) : (
                <div className="justify-center flex mt-20">
                  <Spinner size="35" color="706FD3" />
                </div>
              )}
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
