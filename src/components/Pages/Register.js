import React, { useContext, useState } from "react";

import { register } from "../../utils/user";
import { Context } from "../Store";
import { regexEmail, regexUsername, regexPassword } from "../../utils/constants";

import Logo from "../../assets/Logo";
import Spinner from "../../assets/Spinner";
import Message from "../utilityComponents/Message";

export default function Register(props) {
  const [, setState] = useContext(Context);
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const registerUser = async () => {
    if (!email.match(regexEmail)) return setError("L'adresse e-mail doit être valide");
    if (!username.match(regexUsername)) return setError("Le nom d'utilisateur doit être valide");
    if (!password.match(regexPassword))
      return setError(
        "Le mot de passe doit contenir au moins 8 caractères, dont un chiffre, une majuscule et une minuscule"
      );
    setIsLoading(true);
    document.getElementById("submitBtn").disabled = true;
    if (password !== password2) {
      setIsLoading(false);
      document.getElementById("submitBtn").disabled = false;
      return setError("Les mots de passe doivent correspondre");
    }
    let myRegister = await register({ username, email, password });
    setIsLoading(false);
    document.getElementById("submitBtn").disabled = false;
    if (myRegister.error) {
      if (myRegister.error.error) return setError(myRegister.error.error);
      setError(myRegister.error);
      if (error !== "") {
        document.getElementById("errorMsg").classList.add("animate-bounce");
        await new Promise((r) => setTimeout(r, 1500));
        document.getElementById("errorMsg").classList.remove("animate-bounce");
      }
    } else {
      localStorage.setItem("user", JSON.stringify(myRegister.user));
      localStorage.setItem("token", myRegister.token);
      setState(myRegister);
    }
  };

  document.title = "Register - Coding Ideas";

  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-50 py-2 px-4 sm:px-6 lg:px-8">
      <div className="max-w-lg w-full space-y-8">
        <div>
          <h1 className="font-black text-4xl md:text-6xl text-purple-700 mb-4">Coding Ideas</h1>
          <Logo className="block center m-auto mb-0 w-32 sm:w-64 h-16 sm:h-30" />
          <h2 className="mt-6 text-center text-2xl md:text-3xl font-extrabold text-gray-900">Créez votre compte</h2>
          <p className="mt-2 text-center text-sm font-medium text-gray-600">
            et découvrez des milliers d'idées à développer
          </p>
        </div>
        <form
          className="mt-8 space-y-6"
          onSubmit={(e) => {
            e.preventDefault();
            registerUser();
          }}
        >
          <div className="rounded-md shadow-sm -space-y-px">
            <div>
              <label htmlFor="username" className="sr-only">
                Nom d'utilisateur
              </label>
              <input
                id="username"
                name="username"
                type="text"
                autoComplete="username"
                required
                pattern="^[a-zA-Z0-9]+([_-]?[a-zA-Z0-9]){4,}$"
                title="Le nom d'utilisateur doit être valide et faire au moins 5 caractères"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="Nom d'utilisateur"
              />
            </div>
            <div>
              <label htmlFor="email-address" className="sr-only">
                Adresse e-mail
              </label>
              <input
                id="email-address"
                name="email"
                type="email"
                autoComplete="email"
                required
                title="Veuillez saisir une adresse e-mail correcte"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="Adresse e-mail"
              />
            </div>
            <div>
              <label htmlFor="password" className="sr-only">
                Mot de passe
              </label>
              <input
                id="password"
                name="password"
                type="password"
                autoComplete="current-password"
                required
                pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$"
                title="Le mot de passe doit contenir au moins 8 caractères, dont un chiffre, une majuscule et une minuscule"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="Mot de passe"
              />
            </div>
            <div>
              <label htmlFor="password2" className="sr-only">
                Confirmez votre mot de passe
              </label>
              <input
                id="password2"
                name="password2"
                type="password"
                autoComplete="current-password"
                required
                pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$"
                title="Le mot de passe doit contenir au moins 8 caractères, dont un chiffre, une majuscule et une minuscule"
                value={password2}
                onChange={(e) => setPassword2(e.target.value)}
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="Confirmez votre mot de passe"
              />
            </div>
          </div>
          {error ? <Message message={error} setError={setError} /> : null}
          <div className="flex items-center justify-between font-medium">
            <div></div>
            <div className="text-sm">
              <a href="/login" className="">
                Déjà un compte ? <span className="font-semibold text-purple-700">Se connecter</span>
              </a>
            </div>
          </div>

          <div>
            <button
              type="submit"
              id="submitBtn"
              className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-purple-700 hover:bg-purple-800 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              <span className="absolute left-0 inset-y-0 flex items-center pl-3">
                <svg
                  className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                  aria-hidden="true"
                >
                  <path
                    fillRule="evenodd"
                    d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z"
                    clipRule="evenodd"
                  />
                </svg>
              </span>
              {isLoading ? <Spinner size="20" /> : "Créer mon compte"}
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
