import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";

import { getPostsList } from "../../utils/posts";
import { Context } from "../Store";
import Spinner from "../../assets/Spinner";
import IdeaCard from "../utilityComponents/IdeaCard";
import Message from "../utilityComponents/Message";

export default function Home() {
  const [state] = useContext(Context);
  const [postsList, setPostsList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState("");

  useEffect(() => {
    if (isSubscribedTo()) {
      setIsLoading(true);
      getPostsList(state.user.subscribedTo, state.token)
        .then((posts) => {
          setIsLoading(false);
          posts = posts.filter((post) => !post.error);
          setPostsList(
            posts.sort((x, y) => {
              return Date.parse(y.post.updatedAt) - Date.parse(x.post.updatedAt);
            })
          );
        })
        .catch((error) => {
          setError(error);
        });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state]);

  function isSubscribedTo() {
    if (state.user && state.user.subscribedTo && state.user.subscribedTo.length !== 0) return true;
    return false;
  }

  document.title = "Home - Coding Ideas";

  return (
    <div>
      {error ? <Message message={error} setError={setError} /> : null}
      <div id="homeTitle" className="py-5 break-words">
        <h1 className="text-3xl sm:text-5xl">La liste des idées qui m'intéressent</h1>
      </div>
      <div className="flex justify-center">
        <div className="w-4/5 mb-5">
          {!isLoading ? (
            isSubscribedTo() ? (
              postsList.map((post) => {
                return post.error ? (
                  () => {
                    setError(post.error);
                    return null;
                  }
                ) : (
                  <div key={post.post._id} className="mb-6">
                    <IdeaCard post={post.post} />
                  </div>
                );
              })
            ) : (
              <div className="my-5">
                <h3 className="text-xl ">Vous n'êtes pas encore abonné à des idées...</h3>
                <Link to="ideas">
                  <p className="text-purple-700 text-lg font-bold">Mais vous pouvez y remédier en cliquant ici !</p>
                </Link>
              </div>
            )
          ) : (
            <div className="justify-center flex mt-20">
              <Spinner size="35" color="706FD3" />
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
