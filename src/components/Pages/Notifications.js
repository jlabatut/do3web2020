import React, { useEffect, useState, useContext } from "react";

import { Context } from "../Store";
import { getAllNotificationsByUsername } from "../../utils/notifications";

import Message from "../utilityComponents/Message";
import Spinner from "../../assets/Spinner";
import { Link } from "react-router-dom";

export default function Profile(props) {
  const [state] = useContext(Context);
  const [notifications, setNotifications] = useState([]);
  const [error, setError] = useState("");
  const [info, setInfo] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    getAllNotificationsByUsername(state.user.username, state.token)
      .then((notifs) => {
        setIsLoading(false);
        setNotifications(
          notifs.notifications.sort((x, y) => {
            return Date.parse(y.createdAt) - Date.parse(x.createdAt);
          })
        );
      })
      .catch((e) => {
        setIsLoading(false);
        setError(e);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state]);

  return (
    <div>
      {info ? <Message message={info} type="success" setError={setInfo} /> : null}
      {error.length !== 0 ? <Message message={error} setError={setError} /> : null}

      <div className="flex justify-center">
        <div className="w-full sm:w-4/5">
          <div id="homeTitle" className="py-5 break-words text-left">
            <h1 className="text-3xl sm:text-5xl font-extrabold uppercase mx-5 sm:mx-0">Notifications</h1>
          </div>
          {!isLoading ? (
            <div className=" flex flex-col h-full">
              {notifications.length === 0 ? (
                <div>
                  Patience... <br />
                  Vous n'avez pas encore de notifications, abonnez-vous à des tags ou des idées pour reçevoir les
                  nouveautés
                </div>
              ) : (
                <div className="">
                  <ul>
                    {notifications.map((notif) => {
                      return (
                        <div key={notif._id} className="mb-4">
                          {notif.link ? (
                            <Link to={notif.link}>
                              <NotificationCard notif={notif} />
                            </Link>
                          ) : (
                            <NotificationCard notif={notif} />
                          )}
                        </div>
                      );
                    })}
                  </ul>
                </div>
              )}
            </div>
          ) : (
            <div className="justify-center flex mt-20">
              <Spinner size="35" color="706FD3" />
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

function NotificationCard({ notif }) {
  const formatDate = () => {
    let date = new Date(notif.createdAt);
    return `${`${date.getDate()}/${
      date.getMonth() + 1
    }`}/${date.getFullYear()} - ${date.getHours()}:${date.getMinutes()}`;
  };

  return (
    <div className="flex flex-col flex-wrap bg-gray-200 rounded-lg px-2 py-2 text-left">
      <div className="flex items-center">
        <div className="min-w-max">{formatDate()}&nbsp;&nbsp;&nbsp;</div>
        <div className="text-purple-700 max-w-max font-extrabold text-lg">{notif.title}</div>
      </div>
      <div className="flex items-center font-medium">{notif.content}&nbsp;</div>
      {notif.tags && notif.tags.length > 0 && (
        <div className="flex flex-row items-center">
          {notif.tags.map((tag) => {
            return (
              <div key={tag._id} className="mx-2 mt-2 px-1 text-center bg-purple-700 rounded text-white">
                {tag.title}
              </div>
            );
          })}
          &nbsp;
        </div>
      )}
    </div>
  );
}
