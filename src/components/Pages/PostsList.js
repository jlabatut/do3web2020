import React, { useContext, useState, useEffect } from "react";

import { getAllPosts } from "../../utils/posts";
import { Context } from "../Store";
import Spinner from "../../assets/Spinner";
import IdeaCard from "../utilityComponents/IdeaCard";
import Message from "../utilityComponents/Message";
import TagSearch from "../utilityComponents/TagSearch";
import { Redirect } from "react-router-dom";

export default function PostsList() {
  const [state] = useContext(Context);
  const [ideas, setIdeas] = useState([]);
  const [error, setError] = useState("");
  const [info, setInfo] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [redirect, setRedirect] = useState("");
  const [URLTagsList] = useState([]);

  useEffect(() => {
    if (document.cookie && document.cookie.includes("PostInfos")) {
      const value = `; ${document.cookie}`;
      const postInfo = value.split(`; PostInfos=`)[1];
      setInfo(postInfo);
    }
    setIsLoading(true);
    getAllPosts(state.token).then((posts) => {
      setIsLoading(false);
      if (posts.error) {
        return setError(posts.error.error);
      }
      setIdeas(
        posts.posts.sort((x, y) => {
          return Date.parse(y.createdAt) - Date.parse(x.createdAt);
        })
      );
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  document.title = "Les dernières idées - Coding Ideas";

  return (
    <div className="mt-6">
      {redirect ? <Redirect to={redirect} /> : null}
      {info ? <Message message={info} type="success" /> : null}
      <div id="title" className="pb-5 break-words">
        <h1 className="text-3xl sm:text-4xl">Les dernières idées proposées</h1>
      </div>
      <div className="flex justify-center mb-5">
        <div className="w-11/12 sm:w-4/5">
          <div className="mb-5">
            <TagSearch
              token={state.token}
              setError={setError}
              setIsLoading={setIsLoading}
              setRedirect={setRedirect}
              URLTagsList={URLTagsList}
            />
          </div>
          {error.length !== 0 ? <Message message={error} setError={setError} /> : null}
          {!isLoading ? (
            ideas.map((post) => {
              return post.error ? (
                setError(post.error)
              ) : (
                <div key={post._id} className="mb-6">
                  <IdeaCard post={post} />
                </div>
              );
            })
          ) : (
            <div className="justify-center flex mt-20">
              <Spinner size="35" color="706FD3" />
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
