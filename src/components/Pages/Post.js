import React, { useContext, useState, useEffect } from "react";
import { Redirect, Link } from "react-router-dom";

import { getPost, deletePost } from "../../utils/posts";
import { deleteComment, getCommentsFromPost, createComment } from "../../utils/comments";
import { getUser, subscribeToPost } from "../../utils/user";
import { Context } from "../Store";
import Spinner from "../../assets/Spinner";
import IdeaCard from "../utilityComponents/IdeaCard";
import Message from "../utilityComponents/Message";
import NotFound from "./NotFound";

export default function Post(props) {
  const [state] = useContext(Context);
  const [post, setPost] = useState();
  const [comments, setComments] = useState([]);
  const [newComment, setNewComment] = useState("");
  const [isSubscribed, setIsSubscribed] = useState(false);
  const [error, setError] = useState("");
  const [info, setInfo] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [isNotFound, setIsNotFound] = useState(false);
  const [redirect, setRedirect] = useState("");

  useEffect(() => {
    if (document.cookie && document.cookie.includes("PostInfos")) {
      const value = `; ${document.cookie}`;
      const postInfo = value.split(`; PostInfos=`)[1];
      setInfo("");
      setInfo(postInfo);
    }
    setIsLoading(true);
    getPost(props.match.params.id, state.token).then((data) => {
      setIsLoading(false);
      if (data.error) {
        if (data.errorCode && data.errorCode === 404) {
          return setIsNotFound(true);
        }
        return setError(data.error);
      }
      setPost(data.post);
      if (state.user.subscribedTo.includes(data.post._id)) setIsSubscribed(true);
      document.title = data.post.title + " - Coding Ideas";
      setIsLoading(true);
      getCommentsFromPost(data.post._id, state.token).then((comments) => {
        setIsLoading(false);
        if (comments.error) return setError(comments.error);
        setComments(
          comments.comments.sort((x, y) => {
            return Date.parse(x.updatedAt) - Date.parse(y.updatedAt);
          })
        );
      });
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const toggleSubscribe = async () => {
    document.getElementById("subscribeBtn").disabled = true;
    let action = isSubscribed ? "unsubscribe" : "subscribe";
    let resSub = await subscribeToPost(state.user.username, post._id, action, state.token);
    if (resSub.error) {
      setError();
      return setError(resSub.error);
    }
    let updateUser = await getUser(state.user.username, state.token);
    let updatePost = await getPost(post._id, state.token);
    if (updateUser.error || updatePost.error) {
      setError();
      return setError(updateUser.error || updatePost.error);
    }
    setIsSubscribed(!isSubscribed);
    document.getElementById("subscribeBtn").disabled = false;
  };

  const deleteIdea = async () => {
    if (!window.confirm("Êtes-vous sûr de vouloir supprimer votre idée ?")) return null;
    setIsLoading(true);
    let deleted = await deletePost(post._id, state.token);
    if (deleted.error) {
      setIsLoading(false);
      setError();
      return setError(deleted.error);
    }
    let d = new Date();
    d.setTime(d.getTime() + 2000);
    document.cookie = `PostInfos=Votre idée a bien été supprimée;expires=${d.toUTCString()};path=/`;
    setIsLoading(false);
    return setRedirect(`/ideas/`);
  };

  const deleteOwnComment = async (comment) => {
    if (!window.confirm("Êtes-vous sûr de vouloir supprimer votre commentaire ?")) return null;
    setIsLoading(true);
    let deleted = await deleteComment(comment._id, state.token);
    if (deleted.error) {
      setIsLoading(false);
      setError();
      return setError(deleted.error);
    }
    setComments(comments.filter((com) => com !== comment));
    setInfo("");
    setInfo("Votre commentaire a bien été supprimé");
    setIsLoading(false);
    return null;
  };

  const sendComment = async () => {
    setIsLoading(true);
    let sentComment = await createComment({ originalPost: post._id, content: newComment }, state.token);
    if (sentComment.error) {
      setIsLoading(false);
      return setError(sentComment.error);
    }
    setComments((comments) => [...comments, sentComment.comment]);
    setInfo("");
    setInfo("Votre commentaire a été publié !");
    setNewComment("");
    setIsLoading(false);
    return null;
  };

  const isPostOwner = () => {
    return post.author.username === state.user.username;
  };

  const isCommentOwner = (owner) => {
    return owner === state.user.username;
  };

  return (
    <div className="mt-6">
      {isNotFound && <NotFound />}
      {redirect ? <Redirect to={redirect} /> : null}
      {info ? <Message message={info} type="success" /> : null}
      {error ? <Message message={error} setError={setError} /> : null}
      <div className="flex justify-center mb-4">
        <div className="w-11/12 sm:w-4/5">
          {!isLoading ? (
            post && (
              <div className="">
                <IdeaCard post={post} isList={false} isSubscribed={isSubscribed} toggleSubscribe={toggleSubscribe} />
                {isPostOwner() && (
                  <div className="flex justify-evenly flex-wrap pt-2 text-base bg-gray-300">
                    <button
                      onClick={() => setRedirect(`/idea/${post._id}/edit`)}
                      className={`shadow flex w-28 mb-2 justify-center border-blue-500 border-2 rounded-full focus:outline-none px-3 py-1 text-blue-500 hover:bg-blue-300 hover:text-white font-bold`}
                    >
                      <span>Modifier</span>
                    </button>
                    <button
                      onClick={deleteIdea}
                      className={`shadow flex w-28 mb-2 justify-center border-red-700 border-2  rounded-full focus:outline-none px-3 py-1 text-red-700 hover:bg-red-500 hover:text-white font-bold`}
                    >
                      <span>Supprimer</span>
                    </button>
                  </div>
                )}
                <div className="my-8 flex-col text-left justify-start">
                  <div>
                    {comments && comments.length > 0
                      ? comments.map((comment) => {
                          return (
                            <div key={comment._id} id={comment._id}>
                              <Comment comment={comment} />
                              {isCommentOwner(comment.author.username) && (
                                <div className="flex justify-evenly flex-wrap pt-2 text-base bg-gray-300">
                                  <button
                                    onClick={() => deleteOwnComment(comment)}
                                    className={`shadow flex w-28 mb-2 justify-center border-red-700 border-2  rounded-full focus:outline-none px-3 py-1 text-red-700 hover:bg-red-500 hover:text-white font-bold`}
                                  >
                                    <span>Supprimer</span>
                                  </button>
                                </div>
                              )}
                            </div>
                          );
                        })
                      : null}
                  </div>
                </div>
                <div>
                  <form
                    id="sendCommentForm"
                    onSubmit={(e) => {
                      e.preventDefault();
                      sendComment();
                    }}
                  >
                    <div>
                      <label htmlFor="contentcontent" className="sr-only">
                        Contenu du commentaire
                      </label>
                      <textarea
                        id="contentcontent"
                        name="content"
                        type="text"
                        required
                        minLength="20"
                        rows="3"
                        title="Contenu du commentaire"
                        value={newComment}
                        onChange={(e) => setNewComment(e.target.value)}
                        className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                        placeholder={`${
                          comments.length === 0 ? "Soyez le premier à réagir à cette idée !" : "Commentez cette idée..."
                        }`}
                      />
                    </div>
                  </form>
                  <div className="p-2 bg-gray-200 text-gray-600 font-bold hover:bg-gray-300 hover:text-gray-700">
                    <button
                      type="submit"
                      id="submitBtn"
                      form="sendCommentForm"
                      className="group relative flex justify-center mx-auto py-2 px-2 sm:px-4 border border-transparent text-sm font-medium rounded-md text-white bg-purple-700 hover:bg-purple-800 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                    >
                      Publier mon commentaire
                    </button>
                  </div>
                </div>
              </div>
            )
          ) : (
            <div className="justify-center flex mt-20">
              <Spinner size="35" color="706FD3" />
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

function Comment({ comment }) {
  const formatDate = () => {
    let date = new Date(comment.createdAt);
    return `${`${date.getDate()}/${date.getMonth() + 1}`}/${date.getFullYear()}`;
  };

  return (
    <div className="bg-gray-300 text-gray-700 rounded text-center min-w-auto text-sm mt-6">
      <div className="overflow-ellipsis justify-end">
        <p className="mt-1 px-3 pb-6 pt-3 text-gray-600 text-sm text-justify whitespace-pre-wrap break-words">
          {comment.content}
        </p>
      </div>
      <div className="flex justify-end text-sm p-2 bg-gray-200 text-gray-600 font-bold hover:bg-gray-300 hover:text-gray-700">
        <p>
          Commentaire de{" "}
          {comment.author ? (
            <Link to={comment.author ? `/profile/${comment.author.username}` : "#"} className="text-purple-700">
              {comment.author.username}
            </Link>
          ) : (
            "utilisateur supprimé"
          )}{" "}
          du {formatDate()}
        </p>
      </div>
    </div>
  );
}
