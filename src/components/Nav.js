import React, { useState, useEffect, useContext } from "react";
import { Link, useLocation } from "react-router-dom";
import Logo from "../assets/Logo";
import { Context } from "./Store";
import NotificationsNoneIcon from "@material-ui/icons/NotificationsNone";

export default function Nav() {
  const [state] = useContext(Context);
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const location = useLocation().pathname;
  const switchMenu = () => {
    isMenuOpen ? setIsMenuOpen(false) : setIsMenuOpen(true);
  };
  useEffect(() => {
    setIsMenuOpen(false);
  }, [location]);
  return (
    <nav className="bg-purple-900">
      <div className="max-w-7xl mx-auto px-2 sm:px-6 lg:px-8">
        <div className="relative flex items-center justify-between h-16">
          <div className="absolute inset-y-0 left-0 flex items-center sm:hidden">
            <button
              className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white"
              aria-expanded="false"
              onClick={switchMenu}
            >
              <span className="sr-only">Open main menu</span>
              <svg
                className={`${isMenuOpen ? "hidden" : "block"} h-6 w-6`}
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                aria-hidden="true"
              >
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h16" />
              </svg>
              <svg
                className={`${isMenuOpen ? "block" : "hidden"} h-6 w-6`}
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                aria-hidden="true"
              >
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 18L18 6M6 6l12 12" />
              </svg>
            </button>
          </div>
          <div className="flex-1 flex items-center sm:items-stretch sm:justify-start">
            <div className="flex-shrink-0 flex items-center">
              <Link to="/">
                <Logo className="block h-8 w-auto ml-14 sm:ml-0" />
              </Link>
            </div>
            <div className="hidden sm:block text-white whitespace-nowrap self-center ml-3 font-extrabold uppercase">
              <Link to="/">Coding Ideas</Link>
            </div>
            <div className="hidden sm:block sm:ml-6">
              <div className="flex space-x-4">
                <Link
                  to="/"
                  className={`${
                    location === "/" ? "bg-gray-900 text-white" : "text-gray-300 hover:bg-gray-700 hover:text-white"
                  } px-3 py-2 rounded-md text-sm font-medium"`}
                >
                  Accueil
                </Link>
                <Link
                  to="/ideas"
                  className={`${
                    location === "/ideas" || location.includes("/idea/") || location.includes("/ideas")
                      ? "bg-gray-900 text-white"
                      : "text-gray-300 hover:bg-gray-700 hover:text-white"
                  } px-3 py-2 rounded-md text-sm font-medium"`}
                >
                  Idées
                </Link>
              </div>
            </div>
          </div>
          <div className="absolute inset-y-0 right-0  items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0 hidden sm:flex">
            <Link
              to="/newIdea"
              className={`${
                location === "/newIdea" ? "bg-gray-900 text-white" : "text-gray-300 hover:bg-gray-700 hover:text-white"
              } px-3 py-2 rounded-md text-sm font-medium"`}
            >
              <span className="whitespace-nowrap">J'ai une</span> <span className="whitespace-nowrap">idée !</span>
            </Link>
            <Link
              to={`/profile/${state.user.username}`}
              className={`${
                location === `/profile/${state.user.username}`
                  ? "bg-gray-900 text-white"
                  : "text-gray-300 hover:bg-gray-700 hover:text-white"
              } px-3 py-2 rounded-md text-sm font-medium"`}
            >
              Mon compte
            </Link>
          </div>
          <Link
            to="/notifications"
            className={`${
              location === "/notifications"
                ? "bg-gray-900 text-white"
                : "text-gray-300 hover:bg-gray-700 hover:text-white"
            } px-3 py-2 rounded-md text-sm font-medium"`}
          >
            <NotificationsNoneIcon />
          </Link>
          <Link
            to="/logout"
            className={`${
              location === "/logout" ? "bg-gray-900 text-white" : "text-gray-300 hover:bg-gray-700 hover:text-white"
            } px-3 py-2 truncate rounded-md text-sm font-medium"`}
          >
            Déconnexion
          </Link>
        </div>
      </div>
      <div className={`${isMenuOpen ? "block" : "hidden"} sm:hidden`}>
        <div className="px-2 pt-2 pb-3 space-y-1">
          <div className="h-8">
            <Link
              to="/"
              className={`${
                location === "/" ? "bg-gray-900 text-white" : "text-gray-300 hover:bg-gray-700 hover:text-white"
              } px-3 py-2 rounded-md text-sm font-medium"`}
            >
              Accueil
            </Link>
          </div>
          <div className="h-8">
            <Link
              to="/ideas"
              className={`${
                location === "/ideas" || location.includes("/idea/") || location.includes("/ideas")
                  ? "bg-gray-900 text-white"
                  : "text-gray-300 hover:bg-gray-700 hover:text-white"
              } px-3 py-2 rounded-md text-sm font-medium"`}
            >
              Idées
            </Link>
          </div>
          <div className="h-8">
            <Link
              to="/newIdea"
              className={`${
                location === "/newIdea" ? "bg-gray-900 text-white" : "text-gray-300 hover:bg-gray-700 hover:text-white"
              } px-3 py-2 rounded-md text-sm font-medium"`}
            >
              J'ai une idée !
            </Link>
          </div>
          <div className="h-8">
            <Link
              to={`/profile/${state.user.username}`}
              className={`${
                location === `/profile/${state.user.username}`
                  ? "bg-gray-900 text-white"
                  : "text-gray-300 hover:bg-gray-700 hover:text-white"
              } px-3 py-2 rounded-md text-sm font-medium"`}
            >
              Mon compte
            </Link>
          </div>
        </div>
      </div>
    </nav>
  );
}
