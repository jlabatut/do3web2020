import React from "react";

import Store from "./Store";
import Router from "./Router";

function App() {
  return (
    <Store>
      <div style={{ textAlign: "center" }}>
        <Router />
      </div>
    </Store>
  );
}

export default App;
