import React, { useState } from "react";

import { getUser } from "../utils/user";

export const refreshState = async () => {
  return await getUser(JSON.parse(localStorage.getItem("user")).username, localStorage.getItem("token"));
};

const initialState = {
  user: localStorage.getItem("user") ? JSON.parse(localStorage.getItem("user")) : {},
  token: localStorage.getItem("token") ? localStorage.getItem("token") : "",
};

export const Context = React.createContext();

const Store = ({ children }) => {
  const [state, setState] = useState(initialState);
  return <Context.Provider value={[state, setState]}>{children}</Context.Provider>;
};

export default Store;
