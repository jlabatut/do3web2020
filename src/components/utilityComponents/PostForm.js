import React, { useState, useEffect } from "react";

import TagSearch from "../utilityComponents/TagSearch";
import Spinner from "../../assets/Spinner";
import Message from "./Message";

export default function PostForm({
  token,
  postTitle = "",
  postContent = "",
  postTags = [],
  TagsIdList = [],
  setDataToSend,
  onSubmit,
  error,
  setError,
  setRedirect,
  isLoading,
  setIsLoading,
  isEditing = false,
}) {
  const [title, setTitle] = useState(postTitle);
  const [content, setContent] = useState(postContent);
  const [tags, setTags] = useState(postTags);
  const [URLTagsList] = useState(TagsIdList);

  useEffect(() => {
    let tagsId = [];
    tags.map((e) => tagsId.push(e._id));
    setDataToSend({
      title: title.trim() || postTitle,
      content: content.trim() || postContent,
      tags: tagsId,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [title, content, tags, URLTagsList]);

  const sumbitForm = () => {
    onSubmit();
  };

  return (
    <div className="bg-gray-300 text-gray-700 rounded text-center">
      <form
        id="createPostForm"
        className="mt-2 space-y-6"
        onSubmit={(e) => {
          e.preventDefault();
          sumbitForm();
        }}
      >
        <div className="rounded-md shadow-sm -space-y-px">
          <div>
            <label htmlFor="title" className="sr-only">
              Titre
            </label>
            <input
              id="title"
              name="title"
              type="text"
              required
              minLength="5"
              title="Titre de votre idée"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
              placeholder="Le titre de mon idée..."
            />
          </div>
          <div>
            <label htmlFor="contentcontent" className="sr-only">
              Contenu de l'idée
            </label>
            <textarea
              id="contentcontent"
              name="content"
              type="text"
              required
              minLength="20"
              rows="10"
              title="Contenu de l'idée"
              value={content}
              onChange={(e) => setContent(e.target.value)}
              className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
              placeholder="Précisez votre idée..."
            />
          </div>
        </div>
      </form>
      <div className="py-2">
        <div className="mb-2">
          <TagSearch
            token={token}
            URLTagsList={URLTagsList}
            setError={setError}
            setIsLoading={setIsLoading}
            setRedirect={setRedirect}
            isCreateIdea={true}
            setTags={setTags}
          />
        </div>
        {error.length > 0 ? <Message message={error} setError={setError} /> : null}
        <button
          type="submit"
          id="submitBtn"
          form="createPostForm"
          className="group relative flex justify-center w-44 mx-auto py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-purple-700 hover:bg-purple-800 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        >
          {isLoading ? <Spinner size="20" /> : isEditing ? "Modifier mon idée" : "Publier mon idée"}
        </button>
      </div>
    </div>
  );
}
