import React, { useEffect, useState } from "react";

import { getAllTags } from "../../utils/tags";

import SearchIcon from "@material-ui/icons/Search";
import RemoveCircleIcon from "@material-ui/icons/RemoveCircle";
import AddCircleIcon from "@material-ui/icons/AddCircle";

export default function TagSearch({
  token,
  URLTagsList = [],
  setError,
  setIsLoading,
  setRedirect,
  isCreateIdea = false,
  setTags,
}) {
  const [allTags, setAllTags] = useState([]);
  const [selectedTags, setSelectedTags] = useState([]);
  const [searchValue, setSearchValue] = useState("");

  useEffect(() => {
    getAllTags(token)
      .then((allFetchedTags) => {
        if (allFetchedTags.error) {
          return setError(allFetchedTags.error);
        }
        if (URLTagsList.length !== 0) {
          let tempTagsList = [];
          for (let tag of URLTagsList) {
            let tagToAdd = allFetchedTags.tags.find((someTag) => someTag._id === tag);
            if (!tagToAdd) return setError(`Le tag ${tag} n'existe pas`);
            tempTagsList.push(tagToAdd);
          }
          setAllTags(allFetchedTags.tags.filter((a) => !tempTagsList.map((b) => b._id).includes(a._id)));
          setSelectedTags(tempTagsList);
          if (isCreateIdea) setTags(tempTagsList);
        } else {
          setAllTags(allFetchedTags.tags);
        }
      })
      .catch((e) => {
        setError(e);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [URLTagsList]);

  const searchTag = async () => {
    setIsLoading(true);
    let url = "/ideas/";
    for (let e of selectedTags) {
      url += `${e._id}+`;
    }
    url = url.slice(0, -1);
    setIsLoading(false);
    setRedirect(url);
  };

  const addTag = () => {
    let newValue = searchValue.trim();
    if (newValue === "") {
      setSearchValue("");
      return null;
    }
    newValue =
      allTags.find((someTag) => someTag.title === newValue) ||
      selectedTags.find((someTag) => someTag.title === newValue);
    if (!selectedTags.includes(newValue)) {
      if (typeof newValue === "string" || !allTags.includes(newValue)) {
        setError("Veuillez indiquer un tag existant");
      } else {
        setSelectedTags((selectedTags) => [...selectedTags, newValue]);
        setAllTags((allTags) => allTags.filter((tag) => tag !== newValue));
        if (isCreateIdea) setTags((tags) => [...tags, newValue]);
        setSearchValue("");
      }
    } else {
      setSearchValue("");
    }
  };

  const deleteTag = (deletedTag) => {
    setSelectedTags((selectedTags) => selectedTags.filter((tag) => tag !== deletedTag));
    setAllTags((allTags) => [...allTags, deletedTag]);
    if (isCreateIdea) setTags((tags) => tags.filter((tag) => tag !== deletedTag));
  };

  return (
    <div>
      <div
        className={`font-semibold text-purple-700 min-w-max flex ${isCreateIdea ? "justify-start" : "justify-center"}`}
      >
        <div className="text-purple-900 flex max-w-max flex-wrap items-center">
          {!isCreateIdea ? (
            <AddCircleIcon
              className={`mx-2 ${searchValue.length !== 0 ? "cursor-pointer" : "invisible"}`}
              onClick={addTag}
            />
          ) : (
            <div className="mr-2"></div>
          )}
          <input
            id="tagInput"
            type="search"
            className="bg-purple-white shadow rounded border-0 p-2 w-52"
            placeholder={!isCreateIdea ? "Rechercher par tag..." : "Ajouter un tag..."}
            list="data"
            onChange={(e) => setSearchValue(e.target.value)}
            value={searchValue}
          />
          <datalist id="data">
            {allTags.length > 0 &&
              allTags.map((e) => {
                return <option key={e._id} value={e.title} />;
              })}
          </datalist>
          {!isCreateIdea ? (
            <SearchIcon
              className={`flex ${selectedTags.length !== 0 ? "mx-2 cursor-pointer" : "mx-2 invisible"}`}
              onClick={searchTag}
            />
          ) : (
            <AddCircleIcon
              className={`mx-2 ${searchValue.length !== 0 ? "cursor-pointer" : "invisible"}`}
              onClick={addTag}
            />
          )}
        </div>
      </div>
      <div className="pt-3 flex flex-wrap justify-end">
        {selectedTags.map((tag) => {
          return (
            <div
              key={tag._id}
              id={tag._id}
              className="mr-2 my-1 break-words bg-purple-700 text-white p-2 rounded  leading-none flex items-center hover:"
            >
              {tag.title}
              <RemoveCircleIcon
                className="-mr-2 ml-1 -mt-4 w-2 cursor-pointer"
                style={{ fontSize: 15 }}
                onClick={() => deleteTag(tag)}
              />
            </div>
          );
        })}
      </div>
      {URLTagsList.length !== 0 && !isCreateIdea && (
        <div className="mb-5">
          <div className="flex justify-center">
            <div
              onClick={() => setRedirect("/ideas")}
              className="text-purple-900 hover:text-purple-500 font-bold text-base mt-2 cursor-pointer flex justify-center"
            >
              Revenir à la liste de toutes les idées
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
