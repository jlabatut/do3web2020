import React from "react";
import { Link } from "react-router-dom";

export default function IdeaCard({ post, isList = true, isSubscribed, toggleSubscribe }) {
  const formatDate = () => {
    let date = new Date(post.createdAt);
    return `${`${date.getDate()}/${date.getMonth() + 1}`}/${date.getFullYear()}`;
  };

  const CardHead = () => {
    return (
      <div className="overflow-ellipsis justify-end">
        <div className={`flex flex-wrap items-center border-b-2 ${isList ? "justify-start" : "justify-between"}`}>
          {/* {isList ? null : <div className="w-24 ml-1"></div>} */}
          <h1 className="text-2xl px-3 pb-1 pt-3 text-left uppercase text-purple-900 whitespace-pre-wrap truncate font-bold">
            {post.title}
          </h1>
          {isList ? null : (
            <button
              id="subscribeBtn"
              onClick={toggleSubscribe}
              className={`w-24 shadow flex focus:outline-none justify-center border-2 rounded-lg cursor-pointer px-1 py-1 my-2 mx-3 hover:text-white font-bold ${
                isSubscribed
                  ? "border-green-600 text-green-600 hover:bg-green-300"
                  : "border-purple-900 text-purple-900 hover:bg-purple-700 animate-pulse"
              }`}
            >
              {isSubscribed ? "Suivi" : "Suivre l'idée"}
            </button>
          )}
        </div>
        <p className="mt-3 px-3 pb-5 text-gray-600 text-sm text-justify whitespace-pre-wrap break-words">
          {post.content}
        </p>
      </div>
    );
  };

  return (
    <div className="bg-gray-300 text-gray-700 rounded text-center min-w-auto text-sm">
      {isList ? <Link to={`/idea/${post._id}`}>{CardHead()}</Link> : CardHead()}
      <div className="flex flex-wrap justify-between items-center text-left text-lg p-2 bg-gray-200 text-gray-600 font-bold hover:bg-gray-300 hover:text-gray-700">
        <p className="pr-4">
          Idée de{" "}
          {post.author ? (
            <Link to={post.author ? `/profile/${post.author.username}` : "#"} className="text-purple-700">
              {post.author.username}
            </Link>
          ) : (
            "utilisateur supprimé"
          )}
        </p>
        <p className="text-base font-semibold">Postée le {formatDate()}</p>
      </div>
      {post.tags && post.tags.length !== 0 ? (
        <div className="p-2 flex justify-end flex-wrap text-base bg-gray-200 text-gray-600 font-bold hover:bg-gray-300 hover:text-gray-700">
          {post.tags.map((tag) => {
            return (
              <Link to={`/ideas/${tag._id}`} key={tag._id}>
                <div className="mx-2 mt-2 px-1 bg-purple-700 rounded text-white">{tag.title}</div>
              </Link>
            );
          })}
        </div>
      ) : null}
    </div>
  );
}
