import React, { useContext, useEffect, useState } from "react";
import { BrowserRouter, Switch, Route, Redirect, useLocation } from "react-router-dom";

import { Context } from "./Store";
import { getUser } from "../utils/user";

import Nav from "./Nav";
import Login from "./Pages/Login";
import Register from "./Pages/Register";
import ResetPasswordRequest from "./Pages/ResetPasswordRequest";
import ResetPassword from "./Pages/ResetPassword";
import NotFound from "./Pages/NotFound";
import Home from "./Pages/Home";
import CreatePost from "./Pages/CreatePost";
import UpdatePost from "./Pages/UpdatePost";
import PostsList from "./Pages/PostsList";
import PostsListWithTags from "./Pages/PostsListWithTags";
import PostsListAuthor from "./Pages/PostListAuthor";
import Post from "./Pages/Post";
import Profile from "./Pages/Profile";
import Notifications from "./Pages/Notifications";

export default function Router() {
  const [state, setState] = useContext(Context);
  const [error, setError] = useState("");

  function logout() {
    if (localStorage.getItem("user")) localStorage.removeItem("user");
    if (localStorage.getItem("token")) localStorage.removeItem("token");
    setState({ user: {}, token: "" });
    return <Redirect to={"/login"} />;
  }

  function isLogged() {
    if (state.user && state.user.username) return true;
    return false;
  }

  return (
    <BrowserRouter>
      {error ? window.alert(error) : null}
      {isLogged() ? <Nav /> : null}
      <Switch>
        <Route path="/" exact>
          {isLogged() ? <Home /> : <Redirect to={"/login"} />}
        </Route>
        <Route exact path="/ideas" component={isLogged() ? PostsList : Login} />
        <Route path="/ideas/:id" component={isLogged() ? PostsListWithTags : Login} />
        <Route path="/idea/:id/edit" component={isLogged() ? UpdatePost : Login} />
        <Route path="/idea/:id" component={isLogged() ? Post : Login} />
        <Route path="/newIdea" exact>
          {isLogged() ? <CreatePost /> : <Redirect to={"/login"} />}
        </Route>
        <Route path="/login">{isLogged() ? <Redirect to={"/"} /> : <Login />}</Route>
        <Route path="/register">{isLogged() ? <Redirect to={"/"} /> : <Register />}</Route>
        <Route path="/logout">{isLogged() ? logout : <Redirect to={"/login"} />}</Route>
        <Route exact path="/resetPassword">
          {isLogged() ? <Redirect to={"/"} /> : <ResetPasswordRequest />}
        </Route>
        <Route exact path="/notifications/" component={isLogged() ? Notifications : Login} />
        <Route exact path="/profile/:username" component={isLogged() ? Profile : Login} />
        <Route exact path="/profile/:username/ideas" component={isLogged() ? PostsListAuthor : Login} />
        {isLogged() ? (
          <Route path="/resetPassword/:id">
            <Redirect to={"/"} />
          </Route>
        ) : (
          <Route path="/resetPassword/:id" component={ResetPassword}></Route>
        )}
        <Route component={NotFound} />
      </Switch>
      <RefreshState state={state} setState={setState} setError={setError} isLogged={isLogged()} />
    </BrowserRouter>
  );
}

function RefreshState({ state, setState, setError, isLogged }) {
  const location = useLocation();
  useEffect(() => {
    if (isLogged && location.pathname !== "/logout") {
      getUser(state.user.username, state.token)
        .then(({ user }) => {
          setState({ user, token: state.token });
        })
        .catch((error) => {
          setError(error);
        });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location]);
  return null;
}
